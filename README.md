Follow instruction below to navigate through project:
1.Click on Source to find read me file, license file and project example document. check the wikki on the list in the left hand side check the issues on the list in the left hand side.

I used Scource tree to stage the files. 
1.Login into bitbucket
2.create a repository and project (make it public) 
3.choose the language 4.there is a readme default file, make changes. 
5.add commit comments, click commit
6.open source tree stage the change and push it through master 
7.check the commits 
8.Do the same with license file 
9.Go to the change, click on stage in the source tree 
10.right click and reverse commit then push it through master.

Adding wiki: 1.Go to repository setting click on wiki. 
2.Make it public 
3.Enter the details. 
4.Commit.

Adding issues: 1.Go to repositry setting click on issues. 
2.Make it public 
3.Mention the issue and description 
4.We could assign it to an individual. 
5.We could also upload supporting documents.